//
//  HomeUseCases.swift
//  
//
//  Created by Behrad Kazemi on 6/17/22.
//

import Foundation
public protocol HomeUseCases {
  func getBooks(completion: @escaping (Result<[Book], AppError>) -> Void) -> URLSessionDataTask
}
