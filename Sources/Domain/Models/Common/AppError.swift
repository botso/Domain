//
//  AppError.swift
//  
//
//  Created by Behrad Kazemi on 6/16/22.
//

import Foundation
public enum AppError: Error {
    case networkError(Error)
    case dataNotFound
    case jsonParsingError(Error)
}
