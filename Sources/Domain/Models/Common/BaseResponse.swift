//
//  File.swift
//  
//
//  Created by Behrad Kazemi on 6/17/22.
//

import Foundation
public struct BaseResponse<T: Codable>: Codable {
  public let success: Bool
  public let payload: T?
  public let error: BaseError?
}
