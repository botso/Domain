//
//  File.swift
//  
//
//  Created by Behrad Kazemi on 6/17/22.
//

import Foundation

public struct Book: Codable {
  public let book: String
  public let minimum_amount: String
  public let maximum_amount: String
  public let minimum_price: String
  public let maximum_price: String
  public let minimum_value: String
  public let maximum_value: String
}
